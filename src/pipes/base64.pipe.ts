import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'base64',
  pure: false
})
export class Base64Pipe implements PipeTransform {

  transform( value: any ): any {
    value = 'data:image/jpeg;base64,'+value;
    return value;

  }

}
