import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'miles',
})
export class MilesPipe implements PipeTransform {
  transform(value: string) {
    return value.toLocaleString();
  }
}
