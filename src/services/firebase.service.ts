import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { Http } from "@angular/http";
import {
  AlertController,
  ActionSheetController,
  ToastController,
  Platform
} from 'ionic-angular';
import 'rxjs/Rx';
import { Fiesta } from '../interfaces/fiesta';
import { Promotor } from '../interfaces/promotor';
import { Artista } from '../interfaces/artista';
import { Club } from '../interfaces/club';
import { Opciones } from '../interfaces/opciones';
// import { UsuarioService } from "./usuario.service";
// import { TextToSpeech } from '@ionic-native/text-to-speech';
import { AdMobFree, AdMobFreeBannerConfig } from '@ionic-native/admob-free';
import * as moment from 'moment';
import 'moment/locale/es';

@Injectable()
export class FirebaseService {
  modal: any;
  artistas: any[];
  clubs: any[];
  estilos: any[];
  paisesL: any = [];
  paises: any = [];
  ciudades: any = [];
  promotores: any = [];
  fiestas: any = [];
  provincias: any = [];
  artista: Artista;
  fiesta: Fiesta;
  opciones;
  opcionesDatos;
  opcionesRedes;
  opcionesTokens;
  artistasFB: FirebaseListObservable<any[]>;
  clubsFB: FirebaseListObservable<any[]>;
  estilosFB: FirebaseListObservable<any[]>;
  opcionesFB: FirebaseListObservable<any[]>;
  promotoresFB: FirebaseListObservable<any[]>;
  fiestasFB: FirebaseListObservable<any[]>;
  paisesFB: FirebaseListObservable<any[]>;
  ciudadesFB: FirebaseListObservable<any[]>;

  usuarios: any[] = [];
  constructor(
    public db: AngularFireDatabase,
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    public http: Http,
    public plt: Platform,
    private admobFree: AdMobFree
    // public usuarioSrv: UsuarioService,
    // private tts: TextToSpeech
  ) {
    this.artistasFB = db.list('/artistas/', {
      query: { orderByChild: 'nombre' }
    });
    db.list('/artistas/').subscribe((res) => {
      this.artistas = res;
    });
    this.clubsFB = db.list('/clubs/', {
      query: { orderByChild: 'nombre' }
    });
    db.list('/clubs/').subscribe((res) => {
      this.clubs = res;
    });
    this.estilosFB = db.list('/estilos', {
      query: { orderByChild: 'nombre' }
    });
    db.list('/estilos/').subscribe((res) => {
      this.estilos = res;
      this.estilos.sort((a, b) => {
        if (a.nombre < b.nombre) return -1;
        if (a.nombre > b.nombre) return 1;
        return 0;
      })
    });

    this.promotoresFB = db.list('/promotores/', {
      query: { orderByChild: 'nombre' }
    });
    db.list('/promotores/').subscribe((res) => {
      this.promotores = res;
      this.promotores.sort();
    });
    this.fiestasFB = db.list('/fiestas/', {
      query: { orderByChild: 'nombre' }
    });
    db.list('/fiestas/').subscribe((res) => {
      this.fiestas = res;
      this.fiestas.sort((a, b) => {
        let aa = a.datos.fecha.split("-")
        let bb = b.datos.fecha.split("-")
        aa = '20' + aa[2] + '-' + aa[1] + '-' + aa[0];
        bb = '20' + bb[2] + '-' + bb[1] + '-' + bb[0];
        return aa > bb;
      });
    });

    this.opcionesFB = db.list('/opciones/');
    db.list('/opciones/').subscribe((res) => {
      this.opciones = res;
    });
    db.list('/opciones/').subscribe((res) => {
      this.opciones = res;
    });
    db.list('/opciones/datos/').subscribe((res) => {
      this.opcionesDatos = res;
    });
    db.list('/opciones/redes/').subscribe((res) => {
      this.opcionesRedes = res;
    });
    db.list('/opciones/tokens/').subscribe((res) => {
      this.opcionesTokens = res;
    });
    this.paisesFB = db.list('/paises/', {
      query: { orderByChild: 'nombre' }
    });
    db.list('/paises/').subscribe((res) => {
      this.paises = res;
    })
    this.ciudadesFB = db.list('/ciudades/', {
      query: { orderByChild: 'nombre' }
    });
    db.list('/ciudades/').subscribe((res) => {
      this.ciudades = res;
    })

  }

  publicidad = (opcion) => {
    if (this.plt.is('android')) {
      const bannerConfig: AdMobFreeBannerConfig = {
        isTesting: false,
        autoShow: true,
        id: 'ca-app-pub-1868668305627051/5355356356',
      };
      this.admobFree.banner.config(bannerConfig);
      if (opcion == 'abrir') {
        this.admobFree.banner.prepare().then(
          () => {
            this.admobFree.banner.show()
          }
        ).catch(e => console.log(e));
      } else if (opcion == 'cerrar') {
        this.admobFree.banner.hide();
      }
    }
  }


  abrirToast = (mensaje) => {
    let toast = this.toastCtrl.create({
      message: mensaje,
      duration: 3000,
      showCloseButton: true,
      closeButtonText: 'X',
      cssClass: "toast"
    });
    toast.present();
  }

  fechaHoraActual = () => {
    let date;
    date = new Date();
    date = date.getFullYear() + '-' +
      ('00' + (date.getMonth() + 1)).slice(-2) + '-' +
      ('00' + date.getDate()).slice(-2) + ' ' +
      ('00' + date.getHours()).slice(-2) + ':' +
      ('00' + date.getMinutes()).slice(-2) + ':' +
      ('00' + date.getSeconds()).slice(-2);
    return date;
  }

  removeItemFromArr = (arr, item) => {
    let i = arr.indexOf(item);
    if (i !== -1) {
      arr.splice(i, 1);
    }
  }

  timeAgo = (time) => {
    switch (typeof time) {
      case 'number': break;
      case 'string': time = +new Date(time); break;
      case 'object': if (time.constructor === Date) time = time.getTime(); break;
      default: time = +new Date();
    }
    let time_formats = [
      [60, 'segundos', 1], // 60
      [120, '1 minuto', 'En 1 minuto'], // 60*2
      [3600, 'minutos', 60], // 60*60, 60
      [7200, '1 hora', 'En 1 hora'], // 60*60*2
      [86400, 'horas', 3600], // 60*60*24, 60*60
      [172800, '1 dia', 'Mañana'], // 60*60*24*2
      [604800, 'dias', 86400], // 60*60*24*7, 60*60*24
      [1209600, '1 semana', 'Dentro de 1 Semana'], // 60*60*24*7*4*2
      [2419200, 'semanas', 604800], // 60*60*24*7*4, 60*60*24*7
      [4838400, '1 mes', 'Dentro de 1 Mes'], // 60*60*24*7*4*2
      [29030400, 'meses', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
      [58060800, '1 año', 'Dentro de 1 Año'], // 60*60*24*7*4*12*2
      [2903040000, 'años', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
    ];
    let seconds = (+new Date() - time) / 1000, token = '', list_choice = 1;

    if (seconds == 0) {
      return 'Ahora Mismo'
    }
    if (seconds < 0) {
      seconds = Math.abs(seconds);
      token = '';
      list_choice = 2;
    }
    let i = 0, format;
    while (format = time_formats[i++])
      if (seconds < format[0]) {
        if (typeof format[2] == 'string')
          return format[list_choice];
        else
          return token + ' ' + Math.floor(seconds / format[2]) + ' ' + format[1];
      }
    return time;
  }

  numeroAleatorio = (min, max) => {
    return Math.random() * (max - min) + min;
  }
}
