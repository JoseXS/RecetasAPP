import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Device } from '@ionic-native/device';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { Http, Headers } from "@angular/http";
import 'rxjs/Rx';

@Injectable()
export class LogsService {
    logsErrores: FirebaseListObservable<any[]>;
    constructor(
        public platform: Platform,
        private device: Device,
        public db: AngularFireDatabase,
        public http: Http,
    ) {
        this.logsErrores = db.list('/logs/errores/', {
            query: {
              orderByChild: 'nombre',
            }
        })
        
    }

    addLog = (log)=>{
        console.log(log)
        this.logsErrores.push({
        funcion: log.funcion,
        pagina: log.pagina,
        error: log.error,
        fecha: this.fechaHoraActual(),
        });
    }

    fechaHoraActual = ()=>{
        let date;
        date = new Date();
        date = date.getFullYear() + '-' +
            ('00' + (date.getMonth()+1)).slice(-2) + '-' +
            ('00' + date.getDate()).slice(-2) + ' ' +
            ('00' + date.getHours()).slice(-2) + ':' +
            ('00' + date.getMinutes()).slice(-2) + ':' +
            ('00' + date.getSeconds()).slice(-2);
        return date;
      }
}
