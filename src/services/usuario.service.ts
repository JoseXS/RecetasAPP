import { LoginPage } from '../pages/usuario/login';
import { LogsService } from './logs.service';
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Push, PushToken } from '@ionic/cloud-angular';
import { Device } from '@ionic-native/device';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { Geolocation } from '@ionic-native/geolocation';
import { Usuario } from '../interfaces/usuario';
import { HomePage } from '../pages/home/home';

@Injectable()
export class UsuarioService {
  u;
  usuarios: any[] = [];
  usuariosFB: FirebaseListObservable<any[]>;
  usuario: Usuario = {
    datos: {
      nombre: '',
      pais: '',
      foto: '',
      modelo: '',
      ns: '',
      version: '',
      uuid: '',
      logado: false,
      sobreMi: '',
      key: '',
      fechaRegistro: '',
      sexo: '',
      email: '',
      perfilFacebook: ''
    },
    ultimaActualizacion: '',
    ubicacion: {
      lat: '',
      lng: '',
      fecha: '',
      ultimasUbicaciones: []
    },
    favoritos: {
      artistas: [],
      clubs: [],
      fiestas: []
    }
  }
  kmFiesta = {
    kmAprox: 0,
    frase: ''
  }

  constructor(
    public platform: Platform,
    public push: Push,
    private device: Device,
    private ga: GoogleAnalytics,
    public logSrv: LogsService,
    public db: AngularFireDatabase,
    private geo: Geolocation,

  ) {
    this.obtenerUbicacion();
    this.usuariosFB = db.list('/usuarios/');
    db.list('/usuarios/').subscribe((res) => {
      this.usuarios = res;
      if (this.comprobarUsuarioBD() == true) {
        this.usuario.datos = this.u.datos;
        if (this.u.favoritos.artistas) {
          this.usuario.favoritos.artistas = this.u.favoritos.artistas;
        } else {
          this.usuario.favoritos.artistas = [];
        }
        if (this.u.favoritos.clubs) {
          this.usuario.favoritos.clubs = this.u.favoritos.clubs;
        } else {
          this.usuario.favoritos.clubs = [];
        }
        if (this.u.favoritos.fiestas) {
          this.usuario.favoritos.fiestas = this.u.favoritos.fiestas;
        } else {
          this.usuario.favoritos.fiestas = [];
        }
        this.usuario.datos.key = this.u.datos.key;
        this.usuario.datos.logado = true;
      }
    })
  }

  vaciarUsuario = () => {
    this.usuario = {
      datos: {
        nombre: '',
        pais: '',
        foto: '',
        modelo: '',
        ns: '',
        version: '',
        uuid: '',
        logado: false,
        sobreMi: '',
        key: '',
        fechaRegistro: '',
        sexo: '',
        email: '',
        perfilFacebook: ''
      },
      ultimaActualizacion: '',
      ubicacion: {
        lat: '',
        lng: '',
        fecha: '',
        ultimasUbicaciones: []
      },
      favoritos: {
        artistas: [],
        clubs: [],
        fiestas: []
      }
    }
  }

  iniciarNotificaciones = () => {
    this.platform.ready().then(() => {
      if (this.platform.is("android")) {
        this.inicializar_notificaciones();
      }
    })
  }

  obtenerInformacion = () => {
    if (this.platform.is('android')){
      if (this.device.model) {
        this.usuario.datos.uuid = this.device.uuid;
      }
      if (this.device.model) {
        this.usuario.datos.modelo = this.device.model;
      }
      if (this.device.serial) {
        this.usuario.datos.ns = this.device.serial;
      }
      if (this.device.version) {
        this.usuario.datos.version = this.device.version;
      }
    } else {
      this.usuario.datos.uuid = '';
      this.usuario.datos.modelo = '';
      this.usuario.datos.ns = '123456789';
      this.usuario.datos.perfilFacebook = '123456789';
      this.usuario.datos.version = '';
    }
  }

  obtenerUbicacion = () => {
    if (this.platform.is("android")) {
      this.geo.getCurrentPosition().then((resp) => {
        this.usuario.ubicacion.lat = resp.coords.latitude;
        this.usuario.ubicacion.lng = resp.coords.longitude;
        this.usuario.ubicacion.fecha = resp.timestamp;
        if (this.usuario.datos.logado == true) {
          this.usuariosFB.update(this.usuario.datos.key, {
            ultimaActualizacion : this.fechaHoraActual(),
            ubicacion: {
              lat: this.usuario.ubicacion.lat,
              lng: this.usuario.ubicacion.lng,
              fecha: this.usuario.ubicacion.fecha,
              ultimasUbicaciones: this.usuario.ubicacion.ultimasUbicaciones
            },
          })
        }
      }).catch((error) => {
        console.log('Error getting location', error);
      });
    }
  }

  getKilometrosAprox = (lat1, lon1, lat2, lon2, opcion?) => {
    let p = 0.017453292519943295;    // Math.PI / 180
    let c = Math.cos;
    let a = 0.5 - c((lat2 - lat1) * p) / 2 +
      c(lat1 * p) * c(lat2 * p) *
      (1 - c((lon2 - lon1) * p)) / 2;
    let final: any = 12742 * Math.asin(Math.sqrt(a))
    if (opcion) {
      return final.toFixed(0); // 2 * R; R = 6371 km
    } else {
      this.kmFiesta.kmAprox = Number(final.toFixed(0));
    }

  }

  inicializar_notificaciones = () => {
    // this.push.register().then((t: PushToken) => {
    //   return this.push.saveToken(t);
    // }).then((t: PushToken) => {
    //   //console.log('Token saved:', t.token);
    // }).catch( error=>{
    //   //console.log("Error en el registro"+ JSON.stringify(error));
    // })

    // this.push.rx.notification()
    // .subscribe((msg) => {
    //   alert(msg.title + ': ' + msg.text);
    // });
  }

  comprobarUsuarioBD = () => {
    for (let u of this.usuarios) {
      if (u.datos.perfilFacebook == this.usuario.datos.perfilFacebook) {
        localStorage.removeItem('usuario');
        this.u = u;
        return true;
      }
    }
  }

  editarUsuario = () => {
    if (this.usuario.favoritos.artistas.length == 0) {
      this.usuario.favoritos.artistas = '';
    }
    if (this.usuario.favoritos.clubs.length == 0) {
      this.usuario.favoritos.clubs = '';
    }
    if (this.usuario.favoritos.fiestas.length == 0) {
      this.usuario.favoritos.fiestas = '';
    }
    this.obtenerInformacion();
    let MaysPrimera = (string) => {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }
    this.usuario.datos.nombre = MaysPrimera(this.usuario.datos.nombre);
    this.usuariosFB.update(this.usuario.datos.key, {
      datos: {
        nombre: this.usuario.datos.nombre,
        pais: this.usuario.datos.pais,
        foto: this.usuario.datos.foto,
        modelo: this.usuario.datos.modelo,
        ns: this.usuario.datos.ns,
        version: this.usuario.datos.version,
        uuid: this.usuario.datos.uuid,
        logado: this.usuario.datos.logado,
        sobreMi: this.usuario.datos.sobreMi,
        key: this.usuario.datos.key,
        fechaRegistro: this.usuario.datos.fechaRegistro,
        sexo: this.usuario.datos.sexo,
        email: this.usuario.datos.email,
        perfilFacebook: this.usuario.datos.perfilFacebook
      },
      ultimaActualizacion: this.fechaHoraActual(),
      ubicacion: {
        lat: this.usuario.ubicacion.lat,
        lng: this.usuario.ubicacion.lng,
        fecha: this.usuario.ubicacion.fecha,
        ultimasUbicaciones: this.usuario.ubicacion.ultimasUbicaciones
      },
      favoritos: {
        artistas: this.usuario.favoritos.artistas,
        clubs: this.usuario.favoritos.clubs,
        fiestas: this.usuario.favoritos.fiestas
      }
    });
    
  }

  crearUsuario = () => {
    if (this.usuario.favoritos.artistas.length > 0) {
      this.usuario.favoritos.artistas = this.usuario.favoritos.artistas;
    } else {
      this.usuario.favoritos.artistas = '';
    }
    if (this.usuario.favoritos.clubs.length > 0) {
      this.usuario.favoritos.clubs = this.usuario.favoritos.clubs;
    } else {
      this.usuario.favoritos.clubs = '';
    }
    if (this.usuario.favoritos.fiestas.length > 0) {
      this.usuario.favoritos.fiestas = this.usuario.favoritos.fiestas;
    } else {
      this.usuario.favoritos.fiestas = '';
    }
    this.obtenerInformacion();
    this.usuariosFB.push({
      datos: {
        nombre: this.usuario.datos.nombre,
        pais: this.usuario.datos.pais,
        foto: this.usuario.datos.foto,
        modelo: this.usuario.datos.modelo,
        ns: this.usuario.datos.ns,
        version: this.usuario.datos.version,
        uuid: this.usuario.datos.uuid,
        logado: this.usuario.datos.logado,
        sobreMi: this.usuario.datos.sobreMi,
        key: '',
        fechaRegistro: this.fechaHoraActual(),
        sexo: this.usuario.datos.sexo,
        email: this.usuario.datos.email,
        perfilFacebook: this.usuario.datos.perfilFacebook
      },
      ultimaActualizacion: this.fechaHoraActual(),
      ubicacion: {
        lat: this.usuario.ubicacion.lat,
        lng: this.usuario.ubicacion.lng,
        fecha: this.usuario.ubicacion.fecha,
        ultimasUbicaciones: this.usuario.ubicacion.ultimasUbicaciones
      },
      favoritos: {
        artistas: this.usuario.favoritos.artistas,
        clubs: this.usuario.favoritos.clubs,
        fiestas: this.usuario.favoritos.fiestas
      }
    }).then((res) => {
      this.usuario.datos.key = res.key;
      this.usuariosFB.update(this.usuario.datos.key, {
        datos: {
          nombre: this.usuario.datos.nombre,
          pais: this.usuario.datos.pais,
          foto: this.usuario.datos.foto,
          modelo: this.usuario.datos.modelo,
          ns: this.usuario.datos.ns,
          version: this.usuario.datos.version,
          uuid: this.usuario.datos.uuid,
          logado: this.usuario.datos.logado,
          sobreMi: this.usuario.datos.sobreMi,
          key: this.usuario.datos.key,
          fechaRegistro: this.fechaHoraActual(),
          sexo: this.usuario.datos.sexo,
          email: this.usuario.datos.email,
          perfilFacebook: this.usuario.datos.perfilFacebook
        },
        ultimaActualizacion: this.fechaHoraActual(),
      })
    })
  }

  iniciarSesion = () => {
    // Comprobamos si existe en la BD. 
    // Si existe: Traemos los datos
    // Si no existe: Creamos usuario
    if (this.platform.is('android')) {
      this.usuario.datos.logado = true;
      if (this.comprobarUsuarioBD() == true) {
        this.usuario.datos = this.u.datos;
        if (this.usuario.favoritos.artistas.length > 0) {
          this.usuario.favoritos.artistas = this.usuario.favoritos.artistas;
        } else {
          this.usuario.favoritos.artistas = '';
        }
        if (this.usuario.favoritos.clubs.length > 0) {
          this.usuario.favoritos.clubs = this.usuario.favoritos.clubs;
        } else {
          this.usuario.favoritos.clubs = '';
        }
        if (this.usuario.favoritos.fiestas.length > 0) {
          this.usuario.favoritos.fiestas = this.usuario.favoritos.fiestas;
        } else {
          this.usuario.favoritos.fiestas = '';
        }
        if (this.usuario.ubicacion.ultimasUbicaciones.length > 0) {
          this.usuario.ubicacion.ultimasUbicaciones = this.usuario.ubicacion.ultimasUbicaciones;
        } else {
          this.usuario.ubicacion.ultimasUbicaciones = '';
        }
        if (!this.usuario.datos.email) {
          this.usuario.datos.email = '';
        }
        if (!this.usuario.datos.perfilFacebook) {
          this.usuario.datos.perfilFacebook = '';
        }
        this.editarUsuario();
      } else {
        if (this.usuario.favoritos.artistas.length > 0) {
          this.usuario.favoritos.artistas = this.usuario.favoritos.artistas;
        } else {
          this.usuario.favoritos.artistas = '';
        }
        if (this.usuario.favoritos.clubs.length > 0) {
          this.usuario.favoritos.clubs = this.usuario.favoritos.clubs;
        } else {
          this.usuario.favoritos.clubs = '';
        }
        if (this.usuario.favoritos.fiestas.length > 0) {
          this.usuario.favoritos.fiestas = this.usuario.favoritos.fiestas;
        } else {
          this.usuario.favoritos.fiestas = '';
        }
        if (this.usuario.ubicacion.ultimasUbicaciones.length > 0) {
          this.usuario.ubicacion.ultimasUbicaciones = this.usuario.ubicacion.ultimasUbicaciones;
        } else {
          this.usuario.ubicacion.ultimasUbicaciones = '';
        }
        if (!this.usuario.datos.email) {
          this.usuario.datos.email = '';
        }
        if (!this.usuario.datos.perfilFacebook) {
          this.usuario.datos.perfilFacebook = '';
        }
        this.crearUsuario();
      }
    } else {
      this.usuario.datos.logado = true;
      this.usuario.datos.ns = '123456789';
      this.usuario.datos.perfilFacebook = '123456789';
      if (this.comprobarUsuarioBD() == true) {
        this.usuario.datos = this.u.datos;
        this.editarUsuario();
      } else {
        this.usuario.datos.nombre = 'Anonimo';
        this.crearUsuario();
      }
    }
  }



  analytics = (pantalla) => {
    if (this.platform.is('android')) {
      this.ga.startTrackerWithId('UA-105823855-1').then(
        () => {
          this.ga.trackView(pantalla);
        }
      ).catch((e) => {
        console.log('Error starting GoogleAnalytics', e)
        this.logSrv.addLog({funcion: 'analytics', pagina: 'usuario.service', error: e});
      });
    }
  }

  fechaHoraActual = () => {
    let date;
    date = new Date();
    date = date.getFullYear() + '-' +
      ('00' + (date.getMonth() + 1)).slice(-2) + '-' +
      ('00' + date.getDate()).slice(-2) + ' ' +
      ('00' + date.getHours()).slice(-2) + ':' +
      ('00' + date.getMinutes()).slice(-2) + ':' +
      ('00' + date.getSeconds()).slice(-2);
    return date;
  }
}
