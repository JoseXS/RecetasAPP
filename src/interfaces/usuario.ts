export interface Usuario {
    datos: {
        nombre: string,
        pais: string,
        foto: string,
        modelo: string,
        ns: string,
        version: string,
        uuid: string,
        logado: boolean,
        sobreMi: string,
        key: string,
        fechaRegistro: string,
        sexo: string,
        email: string,
        perfilFacebook: string
    },
    ultimaActualizacion: string,
    ubicacion: {
        lat: any,
        lng: any,
        fecha: any,
        ultimasUbicaciones: any
    },
    favoritos: {
        artistas: any,
        clubs: any,
        fiestas: any
    }
}