export interface Opciones {
    "redes": {
        "email" : string,
        "facebook" : {
            "perfil" : string,
            "seguidores" : string
        },
        "instagram" : {
            "perfil" : string,
            "seguidores" : string
        },
        "mixcloud" : {
            "perfil" : string,
            "seguidores" : string
        },
        "soundcloud" : {
            "perfil" : string,
            "seguidores" : string
        },
        "twitter" : {
            "perfil" : string,
            "seguidores" : string
        },
        "youtube" : {
            "perfil" : string,
            "seguidores" : string
        }
    },
    "datos": {
        "titulo" : string,
        "versionAPP" : string
    },
    "tokens" : {
      "facebook" : string,
      "soundcloud" : string
    }
}