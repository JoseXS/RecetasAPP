export interface Club {
  contacto : {
    email: string,
    movil : string,
    nombre : string
  },
  datos : {
    nombre : string,
    aforo : string,
    horario : string,
    estilos : string,
    ultimaActualizacion : string,
    key : string,
    foto : string,
    bio : string,
    slug : string,
    estado : string,
  },
  ubicacion : {
    direccion : string,
    pais : string,
    ciudad : string,
    lat : string,
    lng : string
  },
  estadisticas : {
    favoritos : {
      favoritos : string,
      puesto : string
    },
    votos : {
      puesto : string,
      votos : string
    }
  },
  redesSociales : {
    facebook : {
      perfil : string,
      puesto : string,
      seguidores : string
    },
    instagram : {
      perfil : string,
      puesto : string,
      seguidores : string
    },
    twitter : {
      perfil : string,
      puesto : string,
      seguidores : string
    },
    web : string,
    youtube : {
      perfil : string,
      puesto : string,
      seguidores : string
    }
  }, 
  promotores: string,
  proximasFiestas: string
}