export interface Artista {
    contacto: {
        email: string,
        movil: string,
        nombre: string
    },
    datos : {
        artistaDesde: string,
        bio: string,
        estado: string,
        estilos: string,
        foto: string,
        nombre: string,
        nombreReal: string,
        pais: string,
        sexo: string,
        slug: string,
        key: string,
        ultimaActualizacion: string
    },
    estadisticas : {
        favoritos : {
            favoritos: string,
            puesto: string
        },
        visitas : {
            puesto: string,
            visitas: string
        },
        votos : {
            puesto: string,
            votos: string
        }
    },
    redesSociales : {
        facebook : {
            perfil: string,
            puesto: string,
            seguidores: string
        },
        instagram : {
            perfil: string,
            puesto: string,
            seguidores: string
        },
        mixcloud : {
            perfil: string,
            puesto: string,
            seguidores: string
        },
        soundcloud : {
            perfil: string,
            puesto: string,
            seguidores: string
        },
        twitter : {
            perfil: string,
            puesto: string,
            seguidores: string
        },
        web: string,
        youtube : {
            perfil: string,
            puesto: string,
            seguidores: string
        }
    }
      
}