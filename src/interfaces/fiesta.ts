export interface Fiesta {
    datos : {
      nombre : string,
      fecha : string,
      hora : string,
      fechaMiliSegundos: string,
      ultimaActualizacion : string,
      key : string,
      flyer : string,
      info : string,
      slug : string,
      estado : string,
      club : string,
      promotor : string,
      estilos: string,
      precio: string
    },
    artistas: string,
    club: any,
    estadisticas : {
      favoritos : {
        favoritos : string,
        puesto : string
      },
      votos : {
        puesto : string,
        votos : string
      }
    },
    eventoFacebook : string
  }