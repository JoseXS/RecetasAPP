export interface Promotor {
    "datos": {
        "nombre": string,
        "club": string,
        "foto": string,
        "info": string,
        "ultimaActualizacion": string,
        "slug": string,
        "estilos": string,
        "key": string
    },
    "redesSociales" : {
        "facebook" : {
            "perfil" : string,
            "puesto" : string,
            "seguidores" : string
        },
        "instagram" : {
            "perfil" : string,
            "puesto" : string,
            "seguidores" : string
        },
        "twitter" : {
            "perfil" : string,
            "puesto" : string,
            "seguidores" : string
        },
        "web" : string,
        "youtube" : {
            "perfil" : string,
            "puesto" : string,
            "seguidores" : string
        }
    },
    "contacto" : {
    "email" : string,
    "movil" : string,
    "nombre" : string
    }
}