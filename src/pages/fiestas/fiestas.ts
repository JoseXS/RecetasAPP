import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FirebaseService } from '../../services/firebase.service';
import { FiestaPage } from './fiesta';

@Component({
  selector: 'page-fiestas',
  templateUrl: 'fiestas.html',
})
export class FiestasPage {
  fiestasOriginal;
  buscadorEstado = false;
  segmentos = 'fiestas';
  mapaLat: number = 40.416668;
  mapaLng: number = -3.703840;
  mapaHeight: '600'

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public firebaseSrv: FirebaseService
  ) {
    this.fiestasOriginal = this.firebaseSrv.fiestas;
    let height = window.innerHeight - 55.99
    console.log(document.getElementById('cardMapa'))
    // document.getElementById('mapa').style.height = ''+height+'px';
  }

  abrirFiesta = (fiesta) => {
    this.navCtrl.push(FiestaPage, { fiesta: fiesta });
  }

  busqueda = (ev: any) => {
    this.firebaseSrv.fiestas = this.fiestasOriginal
    let val = ev.target.value;
    if (val && val.trim() != '') {
      this.firebaseSrv.fiestas = this.firebaseSrv.fiestas.filter((item) => {
        console.log(item)
        if (item.datos.estilos == '') {
          return (item.datos.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
            (item.datos.fecha.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
            (item.club.datos.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
            (item.club.ubicacion.ciudad.toLowerCase().indexOf(val.toLowerCase()) > -1)
        } else if (item.datos.estilos !== '') {
          return (item.datos.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
            (item.datos.fecha.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
            (item.club.datos.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
            (item.club.ubicacion.ciudad.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
            (item.datos.estilos.toString().toLowerCase().indexOf(val.toLowerCase()) > -1)
        }
      })
    }
  }

  buscador = (buscadorEstado) => {
    this.buscadorEstado = buscadorEstado;
  }

}
