import { ReproductorPage } from '../reproductor/reproductor';
import { Http } from '@angular/http';
import { ArtistaPage } from '../artistas/artista';
import { Component } from '@angular/core';
import { ModalController, NavController, NavParams } from 'ionic-angular';
import { FirebaseService } from '../../services/firebase.service';
import { UsuarioService } from '../../services/usuario.service';
import { PhotoViewer } from '@ionic-native/photo-viewer';

import * as moment from 'moment';
import 'moment/locale/es';
declare let SC;

@Component({
  selector: 'page-fiesta',
  templateUrl: 'fiesta.html',
})
export class FiestaPage {
  TOKEN_SOUNDCLOUD = "127ccc69200a1864a5dfadb867b4ffe6";
  kmAprox;
  segmentos = 'fiesta';
  diaSemana;
  diaMes;
  fecha;
  estilosFiesta;
  estilosClub;
  iconoMapa = 'assets/images/iconoMapa.png';
  iconoMapaYo = 'assets/images/iconoMapaYo.png';
  artistasFiestas = [];
  userID;
  tracks = [];
  reproduciendo = false;
  player;
  now = Date.now();
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public firebaseSrv: FirebaseService,
    public usuarioSrv: UsuarioService,
    private photoViewer: PhotoViewer,
    public modalCtrl: ModalController,
    public http: Http
  ) {
    console.log(this.usuarioSrv.usuario.ubicacion.lat)

    this.firebaseSrv.publicidad('abrir');
    this.firebaseSrv.fiesta = this.navParams.get("fiesta");
    this.usuarioSrv.analytics('Fiesta-' + this.firebaseSrv.fiesta.datos.slug)
    console.warn('Fiesta', this.firebaseSrv.fiesta)
    this.usuarioSrv.getKilometrosAprox(this.usuarioSrv.usuario.ubicacion.lat, this.usuarioSrv.usuario.ubicacion.lng, this.firebaseSrv.fiesta.club.ubicacion.lat, this.firebaseSrv.fiesta.club.ubicacion.lng);
    this.fecha = this.firebaseSrv.fiesta.datos.fecha.split("-");
    this.diaSemana = moment(this.firebaseSrv.fiesta.datos.fechaMiliSegundos).format("dddd");
    this.diaSemana = this.diaSemana.charAt(0).toUpperCase() + this.diaSemana.slice(1);
    this.diaMes = moment(this.firebaseSrv.fiesta.datos.fechaMiliSegundos).format("MMMM");
    this.diaMes = this.diaMes.charAt(0).toUpperCase() + this.diaMes.slice(1);
    this.estilosFiesta = this.firebaseSrv.fiesta.datos.estilos.toString()
    this.estilosFiesta = this.estilosFiesta.replace(/,/g, ", ");
    this.estilosClub = this.firebaseSrv.fiesta.club.datos.estilos.toString()
    this.estilosClub = this.estilosClub.replace(/,/g, ", ");
    let iFiesta = 0;
    let cuantasComasFiesta = 0;
    let iClub = 0;
    let cuantasComasClub = 0;

    while (iFiesta != -1) {
      iFiesta = this.estilosFiesta.indexOf(",", iFiesta);
      if (iFiesta != -1) {
        iFiesta++;
        cuantasComasFiesta++;
      }
    }
    if (cuantasComasFiesta == 1) {
      this.estilosFiesta = this.estilosFiesta.replace(/, /g, " y ");
    }
    while (iClub != -1) {
      iClub = this.estilosClub.indexOf(",", iClub);
      if (iClub != -1) {
        iClub++;
        cuantasComasClub++;
      }
    }
    if (cuantasComasClub == 1) {
      this.estilosClub = this.estilosClub.replace(/, /g, " y ");
    }
    if (this.firebaseSrv.fiesta.artistas !== '') {
      for (let artista of this.firebaseSrv.fiesta.artistas) {
        for (let ar of this.firebaseSrv.artistas) {
          if (artista == ar.datos.nombre) {
            this.artistasFiestas.push(ar)
          }
        }
      }
    }
    if (this.firebaseSrv.fiesta.artistas.length > 0) {
      console.log("Tenemos Artistas")
      for (let artista of this.artistasFiestas) {
        if (artista.redesSociales.soundcloud.perfil !== '' || artista.redesSociales.soundcloud.perfil !== 'No disponible')
          console.log(artista)

        this.cargarSoundcloud(artista.redesSociales.soundcloud.perfil);

      }
      SC.initialize({
        client_id: '127ccc69200a1864a5dfadb867b4ffe6'
      });
    }
  }


  cargarSoundcloud(user) {
    let urlObtenerID = "http://api.soundcloud.com/users/" + user + "?client_id=" + this.TOKEN_SOUNDCLOUD;
    this.http.get(urlObtenerID)
      .map(resp => resp.json())
      .subscribe(data => {
        this.userID = data.id;
        this.obtenerTracks();

      })
  }

  obtenerTracks() {
    let urlObtenerTracks = 'http://api.soundcloud.com/users/' + this.userID + '/tracks.json?client_id=' + this.TOKEN_SOUNDCLOUD;
    this.http.get(urlObtenerTracks)
      .map(resp => resp.json())
      .subscribe(data => {
        for (var i = data.length - 1; i >= 0; i--) {
          if (i < 3) {
            this.tracks.push(data[i])
          }

        }
        console.log(this.tracks)
      })
  }

reproducirTema = (tema)=>{
  console.log(tema)
  let modal = this.modalCtrl.create(ReproductorPage, { tema: tema });
  modal.present();
}
  // reproducirTema(idTrack) {
  //   if (this.reproduciendo == false) {
  //     let urlPlayTrack = "/tracks/" + idTrack;
  //     SC.stream(urlPlayTrack).then((player) => {
  //       player.play();

  //       this.player = player

  //     });
  //     localStorage.setItem(idTrack, "true");
  //     this.reproduciendo = true;
  //     console.log(this.player)
  //     console.log(this.reproduciendo)
  //     for (let _track of this.tracks) {
  //       if (_track.id == idTrack) {
  //         _track['reproduciendo'] = true;
  //       } else {
  //         _track['reproduciendo'] = false;
  //       }
  //     }
  //     console.log(this.tracks)

  //   } else if (this.reproduciendo == true) {
  //     this.pauseTrack(idTrack);
  //   }
  // }

  // pauseTrack(idTrack) {
  //   this.reproduciendo = false;
  //   this.player.pause();
  //   for (let _track of this.tracks) {
  //     if (_track.id == idTrack) {
  //       _track['reproduciendo'] = false;
  //     } 
  //   }
  // }

  abrirFoto = (url) => {
    this.photoViewer.show(url);
  }

  abrirArtista = (artista) => {
    this.navCtrl.push(ArtistaPage, { artista: artista });
  }

  backButton = () => {
    this.firebaseSrv.publicidad('cerrar');
    this.navCtrl.pop();
  }

}
