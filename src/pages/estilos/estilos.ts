import { UsuarioService } from '../../services/usuario.service';
import { FiestaPage } from '../fiestas/fiesta';
import { FirebaseService } from '../../services/firebase.service';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content } from 'ionic-angular';

@Component({
  selector: 'page-estilos',
  templateUrl: 'estilos.html',
})
export class EstilosPage {
  @ViewChild(Content) content: Content;
  now = Date.now();
  musicaElectronica = {
    house: false,
    techno: false,
    deepHouse: false,
    remember: false,
    comercial: false
  }
  textoBusqueda = '';
  fiestas = [];
  fiestasC = [];
  nFiestas;
  iFiesta;
  pachangueo = true;
  hiphop = true;
  trap = true;
  regueton = true;
  fiestasOriginal;
  filtro = true;
  listado = false;
  palabraFiltro = '';
  segmentos = 'fiestas';
  mapaLat: number = 40.416668;
  mapaLng: number = -3.703840;
  mapaHeight = 600;
  buscadorEstado = false;
  iconoMapa = 'assets/images/iconoMapa.png';
  iconoMapaYo = 'assets/images/iconoMapaYo.png';
  items = [];
  noMoreItemsAvailable = false;
  count = 0;
  fiestasInicial = [];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public firebaseSrv: FirebaseService,
    public usuarioSrv: UsuarioService
  ) {
    this.usuarioSrv.analytics('Estilos')
    this.fiestasOriginal = this.firebaseSrv.fiestas;
    this.nFiestas = 0;
    this.fiestasInicial = [];

    setTimeout(() => {
      this.mapaHeight = window.innerHeight - 100;
    }, 10)

  }

  todas = () => {
    this.fiestasInicial = [];
    this.fiestas = [];
    this.count = 0;
    this.palabraFiltro = '';
    this.firebaseSrv.db.list('/fiestas/').subscribe((res) => {
      this.firebaseSrv.fiestas = res;
      this.listado = true;
      this.filtro = false;
      console.log(this.firebaseSrv.fiestas.length)
      this.firebaseSrv.fiestas.filter((item) => {
        let now = Date.now();
        if ((item.datos.fechaMiliSegundos + 10800000) > now) {
          this.fiestasInicial.push(item)
        }
      })
      this.fiestasInicial.sort((a, b) => {
        return a.datos.fechaMiliSegundos - b.datos.fechaMiliSegundos;
      });
      let now = Date.now();
      for (let i = 0; i < 10; i++) {
        for (let j = 0; j < this.fiestasInicial.length; j++) {
          if (i === j) {
            if (this.fiestasInicial[i].datos.fechaMiliSegundos + 10800000 > now) {
              this.fiestas.push(this.fiestasInicial[i]);
            } else {
              i = i + 1;
              j = j + 1; 
            }
          }
        }
      }
      this.count += 10;
    });
  }

  iScroll = (infiniteScroll) => {
    if (this.fiestasInicial.length > this.count) {
      new Promise((resolve) => {
        return new Promise((resolve) => {
          setTimeout(() => {
            if (this.count > this.fiestas.length) {
              this.count = this.fiestas.length
            }
            for (let i = this.fiestas.length; i < this.count + 10; i++) {
              if (this.fiestasInicial[i]) {
                this.fiestas.push(this.fiestasInicial[i]);
              }
            }
            this.count += 10;
            infiniteScroll.complete();
            resolve();
          }, 200);
        })
      })
    } else {
      infiniteScroll.complete();
    }
  }

  activar = (opcion) => {
    if (opcion == 'musicaElectronica') {
      this.musicaElectronica.deepHouse = !this.musicaElectronica.deepHouse;
      this.musicaElectronica.house = !this.musicaElectronica.house;
      this.musicaElectronica.techno = !this.musicaElectronica.techno;
      this.musicaElectronica.remember = !this.musicaElectronica.remember;
      this.musicaElectronica.comercial = !this.musicaElectronica.comercial;
      this.pachangueo = !this.pachangueo;
      this.hiphop = !this.hiphop;
      this.trap = !this.trap;
      this.regueton = !this.regueton;
    }
  }

  filtrar = (filtro) => {
    this.fiestas = [];
    this.fiestasInicial = [];
    this.count = 0;
    this.firebaseSrv.db.list('/fiestas/').subscribe((res) => {
      this.firebaseSrv.fiestas = res;
      this.firebaseSrv.fiestas.filter((item) => {
        let now = new Date();
        if ((item.datos.fechaMiliSegundos + 10800000) > now) {
          this.fiestasInicial.push(item)
        }
      })
      this.fiestasInicial.sort((a, b) => {
        return a.datos.fechaMiliSegundos - b.datos.fechaMiliSegundos;
      });
    });
    let val = filtro;
    this.palabraFiltro = val;
    if (val && val.trim() != '') {
      this.fiestasInicial = this.fiestasInicial.filter((item) => {
        if (item.datos.estilos !== '') {
          this.listado = true;
          this.filtro = false;
          return (item.datos.estilos.toString().toLowerCase().indexOf(val.toLowerCase()) > -1)
        }
      })
      let now = Date.now();
      for (let i = 0; i < 10; i++) {
        for (let j = 0; j < this.fiestasInicial.length; j++) {
          if (i === j) {
            if (this.fiestasInicial[i].datos.fechaMiliSegundos > now) {
              this.fiestas.push(this.fiestasInicial[i]);
            } else {  
              
            }
          }
        }
      }
      this.count += 10;
    }
  }

  volverFiltro = () => {
    this.listado = false;
    this.filtro = true;
    this.buscadorEstado = false;
    this.content.resize();
  }

  abrirFiesta = (fiesta) => {
    this.navCtrl.push(FiestaPage, { fiesta: fiesta });
  }

  busqueda = (ev: any) => {
    if (this.textoBusqueda.length == 0 && this.palabraFiltro == '') {
      this.todas()
      return;
    }
    this.filtrar(this.palabraFiltro)
    this.fiestasOriginal = this.fiestas;
    let val = ev.target.value;
    if (val && val.trim() != '') {
      this.fiestasInicial = this.fiestasInicial.filter((item) => {
        if (item.datos.estilos == '') {
          return (item.datos.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
            (item.datos.fecha.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
            (item.club.datos.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
            (item.club.ubicacion.ciudad.toLowerCase().indexOf(val.toLowerCase()) > -1)
        } else if (item.datos.estilos !== '') {
          return (item.datos.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
            (item.datos.fecha.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
            (item.club.datos.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
            (item.club.ubicacion.ciudad.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
            (item.datos.estilos.toString().toLowerCase().indexOf(val.toLowerCase()) > -1)
        }
      })
      this.fiestas = this.fiestasInicial;
    }
  }

  buscador = (buscadorEstado) => {
    this.buscadorEstado = buscadorEstado;
    this.content.resize();
  }
}
