import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FirebaseService } from '../../services/firebase.service';
import { UsuarioService } from '../../services/usuario.service';
import { PhotoViewer } from '@ionic-native/photo-viewer';

@Component({
  selector: 'artista',
  templateUrl: 'artista.html'
})
export class ArtistaPage {
  estilosArtista
  bio = [];
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public firebaseSrv: FirebaseService,
    public usuarioSrv: UsuarioService,
    private photoViewer: PhotoViewer
  ) {
    this.firebaseSrv.artista = this.navParams.get("artista");
    this.usuarioSrv.analytics('Artista-'+this.firebaseSrv.artista.datos.nombre)
    this.estilosArtista = this.firebaseSrv.artista.datos.estilos.toString()
    this.estilosArtista = this.estilosArtista.replace(/,/g, ", ");
    this.arreglarBio();
  }

  abrirRedSocial = (perfil)=>{
    let url;
    if (perfil == 'Facebook') {
      url = 'https://www.facebook.com/' + this.firebaseSrv.artista.redesSociales.facebook.perfil;
    } else if (perfil == 'Twitter') {
      url = 'https://www.twitter.com/' + this.firebaseSrv.artista.redesSociales.twitter.perfil;
    } else if (perfil == 'Soundcloud') {
      url = 'https://www.soundcloud.com/' + this.firebaseSrv.artista.redesSociales.soundcloud.perfil;
    } else if (perfil == 'Instagram') {
      url = 'https://www.instagram.com/' + this.firebaseSrv.artista.redesSociales.instagram.perfil;
    } else if (perfil == 'Web') {
      url = this.firebaseSrv.artista.redesSociales.web;
    }
    window.open(url, '_system', 'location=yes');
  }

  arreglarBio = ()=>{
    this.bio = this.firebaseSrv.artista.datos.bio.split('. ');
  }

  
}
