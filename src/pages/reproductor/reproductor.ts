import { Component, Renderer } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
declare let SC;

@Component({
  selector: 'page-reproductor',
  template: `
  <ion-content style="background-color: black; color: white" padding >
    <ion-item style="background-color: black; color: white">
      <ion-avatar item-left>
        <img [src]="reproductor.tema.user.avatar_url">
      </ion-avatar>
      {{reproductor.tema.title | primeraMayuscula}} <br>
      <b>{{reproductor.tema.user.username | primeraMayuscula}}</b>
    </ion-item>

    <ion-icon (click)="play()" *ngIf="reproductor.play == false" class="center" text-center name="md-play" style="font-size: 80px; padding-top: 20px"></ion-icon>
    <ion-icon (click)="pause()" *ngIf="reproductor.play == true" class="center" text-center name="md-pause" style="font-size: 80px; padding-top: 20px"></ion-icon>

    <ion-list>
      <ion-item style="background-color: black; color: white">
        <ion-badge item-left>{{reproductor.tema.minutos}}</ion-badge>
        <ion-range min="0" [max]="reproductor.tema.duration" snaps="false" (ionChange)="actualizarCurrent(); millisToMinutesAndSeconds(reproductor.tema.currentTime)" [(ngModel)]="reproductor.tema.currentTime" color="danger"></ion-range>
        <ion-badge item-right>{{reproductor.tema.duracion}}</ion-badge>
      </ion-item>
    </ion-list>
  </ion-content>
  `
})
export class ReproductorPage {
  TOKEN_SOUNDCLOUD = "127ccc69200a1864a5dfadb867b4ffe6";
  reproduciendo = false;
  background = "#000"
  player;
  actualizaContador;
  reproductor = {
    play: false,
    player: { pause: ''},
    tema: {
      id: '',
      title: '',
      duration: 0,
      duracion: '',
      currentTime: 0,
      artwork_url: '',
      minutos: '0:00',
      user: { 
        username: '',
        avatar_url: ''
      }
    },
  }
  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public renderer: Renderer
  ) {
    SC.initialize({
      client_id: '127ccc69200a1864a5dfadb867b4ffe6'
    });
    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'reproductor', true);
    if (localStorage.getItem('reproductor')) {
      this.reproductor = JSON.parse(localStorage.getItem('reproductor'));
    }
    this.reproductor.play = false
    this.reproductor.tema = this.navParams.get('tema');
    console.log(this.reproductor.tema)
    setTimeout(()=>{
      this.millisToMinutesAndSeconds(this.reproductor.tema.duration, 'ok')
      this.reproductor.tema.minutos = '0:00';
    }, 500)
    
    
  }
  actualizarCurrent = ()=>{
    this.player.controller._currentPosition = this.reproductor.tema.currentTime;
  }
  millisToMinutesAndSeconds = (millis, ok?)=> {
    
    let minutes = Math.floor(millis / 60000);
    let seconds:any = ((millis % 60000) / 1000).toFixed(0);
    if (ok == 'ok') {
      this.reproductor.tema.duracion = minutes + ":" + (seconds < 10 ? '0' : '') + seconds
    } else {
      this.reproductor.tema.minutos = minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
    }
  }

  play=()=> {
    let idTrack = this.reproductor.tema.id;
    if (this.reproductor.play == false) {
      let urlPlayTrack = "/tracks/" + idTrack;
      SC.stream(urlPlayTrack).then((player) => {
        player.play();
        this.player = player;
        // this.reproductor.player = this.player;
        this.reproductor.play = true;
        localStorage.setItem('reproductor', JSON.stringify(this.reproductor));
        this.actualizaContador = setInterval(()=>{
          this.millisToMinutesAndSeconds(this.player.controller._currentPosition)
        }, 1000)
      });
    } else if (this.reproduciendo == true) {
      this.pause();
    }
  }

  pause =()=> {
    this.reproductor.play = false;
    this.player.pause();
    
    console.log(this.player.controller._currentPosition)
    this.millisToMinutesAndSeconds(this.player.controller._currentPosition)
    // this.reproductor.player = this.player;
    localStorage.setItem('reproductor', JSON.stringify(this.reproductor));
    this.player.pause();
    clearInterval(this.actualizaContador)
  }

}
