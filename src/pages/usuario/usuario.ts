import { UsuarioService } from '../../services/usuario.service';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Facebook } from '@ionic-native/facebook';
import { NativeStorage } from '@ionic-native/native-storage';
import { LoginPage } from '../usuario/login';


@Component({
	selector: 'page-usuario',
	templateUrl: 'usuario.html'
})

export class UsuarioPage {

	user: any;
	userReady: boolean = false;

	constructor(
		public navCtrl: NavController,
  		public fb: Facebook,
		public nativeStorage: NativeStorage,
		public usuarioSrv: UsuarioService  
	) {
		console.log(this.usuarioSrv.usuario)
		console.log("Usuario")
	}

}