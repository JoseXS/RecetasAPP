import { EstilosPage } from '../estilos/estilos';
import { SortPipe } from '../../pipes/sort.pipe';
import { FirebaseService } from '../../services/firebase.service';
import { UsuarioService } from '../../services/usuario.service';
// import { HomePage } from '../home/home';
import { Component, ViewChild } from '@angular/core';
import { Facebook } from '@ionic-native/facebook';
import { NativeStorage } from '@ionic-native/native-storage';
import { NavController, Platform } from 'ionic-angular';
import { UsuarioPage } from '../usuario/usuario';
import { Slides } from 'ionic-angular';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  FB_APP_ID: number = 294977164309830;
  estilos;
  @ViewChild(Slides) slides: Slides;
  constructor(
	  public navCtrl: NavController,
  	public fb: Facebook,
    public nativeStorage: NativeStorage,
    public usuarioSrv: UsuarioService,
    public firebaseSrv: FirebaseService,
    public platform: Platform

	) {
    if (this.platform.is('android')) {
      // console.log("Login")
      this.usuarioSrv.obtenerInformacion();
      this.usuarioSrv.obtenerUbicacion();
      this.fb.browserInit(this.FB_APP_ID, "v2.8");
    }
  }

  loginFB = ()=>{
    this.usuarioSrv.obtenerUbicacion();
    let permissions = new Array<string>();
    permissions = ["public_profile"];
    if (this.platform.is('android')) {
      this.fb.login(permissions)
      .then((response)=>{
        let userId = response.authResponse.userID;
        let params = new Array<string>();
        this.fb.api("/me?fields=name,gender,email", params)
        .then((user)=> {
          user.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
          if (user.name) {
            this.usuarioSrv.usuario.datos.nombre = user.name;
          }
          if (user.gender) {
            if (user.gender == 'male') {
              this.usuarioSrv.usuario.datos.sexo = 'Hombre';
            } else if (user.gender == 'female') {
              this.usuarioSrv.usuario.datos.sexo = 'Mujer';
            }
          }
          if (user.email) {
            this.usuarioSrv.usuario.datos.email = user.email;
          }
          this.usuarioSrv.usuario.datos.perfilFacebook = user.id; 
          this.usuarioSrv.usuario.datos.foto = user.picture;
          if (!this.usuarioSrv.usuario.favoritos.artistas) {
            this.usuarioSrv.usuario.favoritos.artistas = '';
          }
          this.nativeStorage.setItem('usuario', 
            { 
              datos: {
                nombre: this.usuarioSrv.usuario.datos.nombre,
                pais: this.usuarioSrv.usuario.datos.pais,
                foto: this.usuarioSrv.usuario.datos.foto,
                modelo: this.usuarioSrv.usuario.datos.modelo,
                ns: this.usuarioSrv.usuario.datos.ns,
                version: this.usuarioSrv.usuario.datos.version,
                uuid: this.usuarioSrv.usuario.datos.uuid,
                logado: true,
                sobreMi: this.usuarioSrv.usuario.datos.sobreMi,
                key: this.usuarioSrv.usuario.datos.key,
                fechaRegistro: this.usuarioSrv.usuario.datos.fechaRegistro,
                ultimaActualizacion: this.usuarioSrv.fechaHoraActual(),
                sexo : this.usuarioSrv.usuario.datos.sexo,
                email: this.usuarioSrv.usuario.datos.email,
                perfilFacebook: this.usuarioSrv.usuario.datos.perfilFacebook
              },
              ubicacion: {
                lat: this.usuarioSrv.usuario.ubicacion.lat,
                lng: this.usuarioSrv.usuario.ubicacion.lng,
                fecha: this.usuarioSrv.usuario.ubicacion.fecha,
                ultimasUbicaciones: this.usuarioSrv.usuario.ubicacion.ultimasUbicaciones
              },
              favoritos: {
                artistas: this.usuarioSrv.usuario.favoritos.artistas,
                clubs: this.usuarioSrv.usuario.favoritos.clubs,
                fiestas: this.usuarioSrv.usuario.favoritos.fiestas,
              }
            }
          )
          .then(()=>{
            this.navCtrl.setRoot(EstilosPage)
            this.nativeStorage.getItem('usuario').then((res)=>{
              // console.log('res despues de logarse inicialmente', res)
              this.usuarioSrv.usuario = res;
              console.log(this.usuarioSrv.usuario);
              this.usuarioSrv.iniciarSesion();
            })
          }, (error)=> {
            console.error(error);
          })
        })
      }, (error)=>{
        console.error(error);
      });
    } else {
      // console.log("Login Browser");
      this.usuarioSrv.iniciarSesion();
      this.navCtrl.setRoot(EstilosPage);
    }
  }

  noIniciarSesion = ()=>{
    this.navCtrl.setRoot(EstilosPage);
  }


  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    let content = document.getElementById('content');
    if (currentIndex == 0) {
      content.style.backgroundColor = '#000';
      content.style.backgroundImage = 'url("assets/images/login/login01.jpg")';
    } else if (currentIndex == 1) {
      content.style.backgroundColor = '#000';
      content.style.backgroundImage = 'url("assets/images/login/login02.jpg")';
    } else if (currentIndex == 2) {
      content.style.backgroundColor = '#000';
      content.style.backgroundImage = 'url("assets/images/login/login03.jpg")';
    }
  }
}