import { EstilosPage } from '../pages/estilos/estilos';
import { LoginPage } from '../pages/usuario/login';
import { UsuarioPage } from '../pages/usuario/usuario';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { FiestasPage } from '../pages/fiestas/fiestas';
import { UsuarioService } from '../services/usuario.service';
import { Facebook } from '@ionic-native/facebook';
import { NativeStorage } from '@ionic-native/native-storage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = EstilosPage;

  pages: Array<{title: string, icon: string, component: any}>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public nativeStorage: NativeStorage,
    public usuarioSrv: UsuarioService,
    public fb: Facebook
  ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      // { title: 'Inicio', icon: 'fa fa-home', component: HomePage },
      // { title: 'Fiestas', icon: 'fa fa-glass', component: FiestasPage },
      { title: 'Fiestas', icon: 'fa fa-glass', component: EstilosPage }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.nativeStorage.getItem('usuario')
      .then((data)=> {
        if (data.datos.logado == true) {
          this.usuarioSrv.usuario = data;
        }
        this.nav.setRoot(EstilosPage);
        this.splashScreen.hide();
      }, (error)=> {
        this.nav.setRoot(LoginPage);
        this.splashScreen.hide();
      });
      this.statusBar.styleDefault();
    });
  }


  openPage(page) {
    this.nav.setRoot(page.component);
  }

  abrirPagina = (pagina)=>{
    if (pagina == 'usuario') 
    this.nav.setRoot(UsuarioPage);
  }

  iniciarSesion = ()=>{
    this.usuarioSrv.obtenerUbicacion();
    let permissions = new Array<string>();
    permissions = ["public_profile"];
    if (this.platform.is('android')) {
      this.fb.login(permissions)
      .then((response)=>{
        let userId = response.authResponse.userID;
        let params = new Array<string>();
        this.fb.api("/me?fields=name,gender,email", params)
        .then((user)=> {
          console.log('Usuario', user);
          console.log(this.usuarioSrv.usuario.ubicacion.lat, this.usuarioSrv.usuario.ubicacion.lng)
          user.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
          if (user.name) {
            this.usuarioSrv.usuario.datos.nombre = user.name;
          }
          if (user.gender) {
            if (user.gender == 'male') {
              this.usuarioSrv.usuario.datos.sexo = 'Hombre';
            } else if (user.gender == 'female') {
              this.usuarioSrv.usuario.datos.sexo = 'Mujer';
            }
          }
          if (user.email) {
            this.usuarioSrv.usuario.datos.email = user.email;
          }
          this.usuarioSrv.usuario.datos.perfilFacebook = user.id; 
          this.usuarioSrv.usuario.datos.foto = user.picture;
          this.nativeStorage.setItem('usuario', 
            { 
              datos: {
                nombre: this.usuarioSrv.usuario.datos.nombre,
                pais: this.usuarioSrv.usuario.datos.pais,
                foto: this.usuarioSrv.usuario.datos.foto,
                modelo: this.usuarioSrv.usuario.datos.modelo,
                ns: this.usuarioSrv.usuario.datos.ns,
                version: this.usuarioSrv.usuario.datos.version,
                uuid: this.usuarioSrv.usuario.datos.uuid,
                logado: true,
                sobreMi: this.usuarioSrv.usuario.datos.sobreMi,
                key: this.usuarioSrv.usuario.datos.key,
                fechaRegistro: this.usuarioSrv.usuario.datos.fechaRegistro,
                ultimaActualizacion: this.usuarioSrv.fechaHoraActual(),
                sexo: this.usuarioSrv.usuario.datos.sexo,
                email: this.usuarioSrv.usuario.datos.email,
                perfilFacebook: this.usuarioSrv.usuario.datos.perfilFacebook
              },
              ubicacion: {
                lat: this.usuarioSrv.usuario.ubicacion.lat,
                lng: this.usuarioSrv.usuario.ubicacion.lng,
                fecha: this.usuarioSrv.usuario.ubicacion.fecha,
                ultimasUbicaciones: this.usuarioSrv.usuario.ubicacion.ultimasUbicaciones
              },
              favoritos: {
                artistas: this.usuarioSrv.usuario.favoritos.artistas,
                clubs: this.usuarioSrv.usuario.favoritos.clubs,
                fiestas: this.usuarioSrv.usuario.favoritos.fiestas,
              }
            }
          )
          .then(()=>{
            // this.nav.setRoot(EstilosPage)
            this.nativeStorage.getItem('usuario').then((res)=>{
              this.usuarioSrv.usuario = res;
              console.log('Usuario despues de LoginFB', this.usuarioSrv.usuario);
              this.usuarioSrv.iniciarSesion();
            })
          }, (error)=> {
            console.error(error);
          })
        })
      }, (error)=>{
        console.error(error);
      });
    } else {
      console.log("Login Browser");
      // this.nav.setRoot(EstilosPage);
      this.usuarioSrv.iniciarSesion();
    }
  }

  cerrarSesion = ()=>{
    if (this.platform.is('android')) {
      this.fb.logout().then(
        (response)=> {
          this.nativeStorage.remove('usuario');
          this.usuarioSrv.vaciarUsuario();
          this.nativeStorage.setItem('usuario', {datos: {logado : false}}).then((res)=>{
            console.log(res)
            this.usuarioSrv.usuario.datos.logado = res.datos.logado;
          })
          
          // this.nav.setRoot(LoginPage)
        }, (error)=>{
          console.log(error);
        }
      );
    } else {
      this.usuarioSrv.vaciarUsuario();
      this.usuarioSrv.usuario.datos.logado = false;
    } 
  }
}
