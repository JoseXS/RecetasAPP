import { ReproductorPage } from '../pages/reproductor/reproductor';
import { MilesPipe } from '../pipes/miles.pipe';
import { EstilosPage } from '../pages/estilos/estilos';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { LoginPage } from '../pages/usuario/login';
import { UsuarioPage } from '../pages/usuario/usuario';

import { FiestasPage } from '../pages/fiestas/fiestas';
import { FiestaPage } from '../pages/fiestas/fiesta';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//servicios
import { FirebaseService } from '../services/firebase.service';
import { UsuarioService } from '../services/usuario.service';
import { LogsService } from '../services/logs.service';

//pipes
import { KeysPipe } from '../pipes/keys.pipe';
import { Base64Pipe } from '../pipes/base64.pipe';
import { TimeAgoPipe } from '../pipes/timeAgo.pipe';
import { TimeAgo2Pipe } from '../pipes/timeAgo2.pipe';

//plugins
import { Camera } from '@ionic-native/camera';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { SocialSharing } from '@ionic-native/social-sharing';
import { AdMobFree } from '@ionic-native/admob-free';
import { Device } from '@ionic-native/device';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { Ionic2RatingModule } from 'ionic2-rating';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { AgmCoreModule } from '@agm/core';
import { AgmSnazzyInfoWindowModule } from '@agm/snazzy-info-window';
import { Calendar } from '@ionic-native/calendar';

import { AngularFireModule } from 'angularfire2';
import { firebaseConfig } from '../config/firebase';
import { AngularFireDatabase } from 'angularfire2/database';
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';
import { CalendarService } from '../services/calendar.service';
import { Geolocation } from '@ionic-native/geolocation';
import { Facebook } from '@ionic-native/facebook';
import { NativeStorage } from '@ionic-native/native-storage';
import { SortPipe } from '../pipes/sort.pipe';
import { GoogleMaps } from '@ionic-native/google-maps';
import { ArtistasPage } from '../pages/artistas/artistas';
import { ArtistaPage } from '../pages/artistas/artista';
import { PrimeraMayusculaPipe } from '../pipes/primeraMayuscula.pipe';

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '038b4d0b'
  },
  'push': {
    'sender_id': '429292327006',
    'pluginConfig': {
      'ios': {
        'badge': true,
        'sound': true
      },
      'android': {
        'iconColor': '#9300fa'
      }
    }
  },
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    FiestasPage,
    FiestaPage,
    LoginPage,
    UsuarioPage,
    EstilosPage,
    KeysPipe,
    Base64Pipe,
    TimeAgoPipe,
    TimeAgo2Pipe,
    MilesPipe,
    SortPipe,
    ArtistasPage,
    ArtistaPage,
    ReproductorPage,
    PrimeraMayusculaPipe
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig.firebase),
    CloudModule.forRoot(cloudSettings),
    Ionic2RatingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAm9mw_ohI-I-giGLaSCjqc04h3dwbw_0c'
    }),
    AgmSnazzyInfoWindowModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    FiestasPage,
    FiestaPage,
    LoginPage,
    UsuarioPage,
    EstilosPage,
    ArtistasPage,
    ArtistaPage,
    ReproductorPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FirebaseService,
    UsuarioService,
    LogsService,
    AngularFireModule,
    AngularFireDatabase,
    Camera,
    PhotoViewer,
    SocialSharing,
    AdMobFree,
    Device,
    GoogleAnalytics,
    TextToSpeech,
    Calendar,
    Geolocation,
    Facebook,
    NativeStorage,
    GoogleMaps,

    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CalendarService
  ]
})
export class AppModule {}
